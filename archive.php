<?php get_header(); ?>
<!-- CONTAINER -->
<div class="container">
	<div class="row">
		<!-- CONTENT -->
		<section class="span8">
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php /* If this is a category archive */ if (is_category()) { ?>
			    Arquivo da Categoria "<?php echo single_cat_title(); ?>"
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			    Arquivo de <?php the_time('j \d\e F \d\e Y'); ?>
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			    Arquivo de <?php the_time('F \d\e Y'); ?>
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			    Arquivo de <?php the_time('Y'); ?>
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			    Arquivo do Autor
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			    Arquivo do Blog
			<?php } ?>
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			<!-- ARTICLE -->
			<article>
				<?php if(current_user_can('edit_posts')): ?>
				<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-mini pull-right"><i class="icon-edit"></i> Editar </a>
				<?php endif ?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<small>Postado por <?php the_author(); ?> em <?php the_time('d, M/Y') ?></small>
				</h3>
				<p><?php the_content(); ?></p>
				<div class="comments">
					<a href="<?php the_permalink(); ?>" class="pull-right">Link direto</a>
					<?php comments_popup_link('Sem Comentários', '1 Comentário', '% Comentários', 'comments-link', ''); ?>
				</div>
			</article><!-- /ARTICLE -->
			<?php endwhile ?>
			
			<!-- PAGING -->
			<ul class="pager">
				<?php
				$prev = get_previous_post();
				if (!empty($prev)): ?>
					<li class="previous"><a href="<?php echo get_permalink($prev->ID); ?>">&larr; <?php echo $prev->post_title; ?></a></li>
				<?php endif; ?>
				<?php
				$next = get_next_post();
				if (!empty($next)): ?>
					<li class="next"><a href="<?php echo get_permalink($next->ID); ?>"><?php echo $next->post_title; ?> &rarr;</a></li>
				<?php endif; ?>
			</ul><!-- /PAGING -->
			
			<?php else: ?>
			<div class="no-content">
				<h1>Vazio?</h1>
				<p>Nenhum artigo está disponível para leitura, tente acessar mais tarde.</p>
			</div>
			<?php endif; ?>
			
		</section><!-- /CONTENT -->

		<!-- SIDEBAR -->
		<?php get_sidebar(); ?>
	</div>
</div><!-- /CONTAINER -->
<?php get_footer(); ?>