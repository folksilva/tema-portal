<?php get_header(); ?>
<!-- CONTAINER -->
<div class="container">
	<div class="row">
		<!-- CONTENT -->
		<section class="span8">
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			<!-- ARTICLE -->
			<article>
				<?php if(current_user_can('edit_posts')): ?>
				<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-mini pull-right"><i class="icon-edit"></i> Editar </a>
				<?php endif ?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h3>
				<p><?php the_content(); ?></p>
			</article><!-- /ARTICLE -->
			<?php endwhile ?>

			<?php else: ?>
			<div class="no-content">
				<h1>Vazio?</h1>
				<p>A página não está disponível para leitura, tente acessar mais tarde.</p>
			</div>
			<?php endif; ?>
			
		</section><!-- /CONTENT -->

		<!-- SIDEBAR -->
		<?php get_sidebar(); ?>
	</div>
</div><!-- /CONTAINER -->
<?php get_footer(); ?>