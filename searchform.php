<form role="search" method="get" id="searchform" class="form-search" action="<?php echo home_url('/'); ?>">
	<div class="input-append">
		<input type="text" class="search-query" name="s" id="s" value="<?php the_search_query(); ?>" placeholder="Pesquisar no portal">
		<button class="btn btn-primary" type="submit"><i class="icon-search icon-white"></i> Buscar</button>
	</div>
</form>