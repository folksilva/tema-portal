<?php get_header(); ?>
<!-- CONTAINER -->
<div class="container">
	<div class="row">
		<!-- CONTENT -->
		<section class="span8">
			<?php query_posts('posts_per_page=1');?>
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			<!-- ARTICLE -->
			<article>
				<?php if(current_user_can('edit_posts')): ?>
				<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-mini pull-right"><i class="icon-edit"></i> Editar </a>
				<?php endif ?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<small>Postado por <?php the_author(); ?> em <?php the_time('d, M/Y') ?></small>
				</h3>
				<p><?php the_content(); ?></p>
				<div class="comments">
					<a href="<?php the_permalink(); ?>" class="pull-right">Link direto</a>
					<?php comments_popup_link('Sem Comentários', '1 Comentário', '% Comentários', 'comments-link', ''); ?>
					<?php wp_list_comments(); ?>
				</div>
			</article><!-- /ARTICLE -->
			<?php endwhile ?>

			<!-- PAGING -->
			<ul class="pager">
				<?php
				$prev = get_previous_post();
				if (!empty($prev)): ?>
					<li class="previous"><a href="<?php echo get_permalink($prev->ID); ?>">&larr; <?php echo $prev->post_title; ?></a></li>
				<?php endif; ?>
				<?php
				$next = get_next_post();
				if (!empty($next)): ?>
					<li class="next"><a href="<?php echo get_permalink($next->ID); ?>"><?php echo $next->post_title; ?> &rarr;</a></li>
				<?php endif; ?>
			</ul><!-- /PAGING -->

			<?php else: ?>
			<div class="no-content">
				<h1>Vazio?</h1>
				<p>Nenhum artigo está disponível para leitura, tente acessar mais tarde.</p>
			</div>
			<?php endif; ?>

		</section><!-- /CONTENT -->

		<!-- SIDEBAR -->
		<?php get_sidebar(); ?>
	</div>
</div><!-- /CONTAINER -->
<?php get_footer(); ?>