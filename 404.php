<?php get_header(); ?>
<!-- CONTAINER -->
<div class="container">
	<div class="row">
		<!-- CONTENT -->
		<section class="span8">
			<div class="hero-unit widget error-widget" data-title="Opss! Algo está errado.">
				<h1><span class="muted">404</span> Não encontrado</h1>
				<p>O que você está procurando não está neste endereço, tente usar a busca.</p>
				<?php get_search_form(true) ?>
			</div>
		</section><!-- /CONTENT -->

		<!-- SIDEBAR -->
		<?php get_sidebar(); ?>
	</div>
</div><!-- /CONTAINER -->
<?php get_footer(); ?>