#Tema do Wordpress para o Portal dos Atletas

Tema criado para uso em um portal interno para os atletas.

##Licença de uso

Este trabalho está licenciado sob uma Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada. Para ver uma cópia desta licença, visite [http://creativecommons.org/licenses/by-sa/3.0/](http://creativecommons.org/licenses/by-sa/3.0/).

O logotipo incluso nos arquivos é propriedade do Sport Club Corinthians Paulista e não pode ser utilizado sem autorização, caso queira usar o logo procure o departamento de marketing do Corinthians.

O conteúdo da pasta bootstrap pertence ao projeto Twitter Bootstrap, consulte [http://twitter.github.com/bootstrap/index.html](http://twitter.github.com/bootstrap/index.html) para obter detalhes de licenciamento.

