<?php 
// Hack para funcionar com o proxy
$default_opts = array(
  'http'=>array(
    'proxy'=>"tcp://127.0.0.1:3128"
  )
);
$default = stream_context_get_default($default_opts);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
		<meta name="author" content="Luiz Fernando da Silva <lfsilva@sccorinthians.com.br>" />
		<meta name="copyright" content="Sport Club Corinthians Paulista" />
		<meta name="description" content="Portal do atleta" />
		<title><?php bloginfo('name'); ?> <?php echo wp_title(); ?></title>
		<link href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" media="screen">
		<link href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>
	</head>
	<body>
		<?php
		if(!is_user_logged_in()){
		?>
			<div class="container">
				<div class="hero-unit widget error-widget" data-title="Opss! Algo está errado.">
					<h1>Acesso não autorizado</h1>
					<p>É preciso estar logado para usar acessar esse portal.</p>
					<a href="<?php echo wp_login_url(); ?>" class="btn btn-large btn-primary">Identifique-se</a>
				</div>
			</div>
		</body>
		<?php die();} else {
			global $current_user;
			get_currentuserinfo();
		} ?>
		<!-- HEADER -->
		<div class="head">
			<div class="container">
				<div class="row-fluid">
					<div class="span1 logo-header">
						<img src="<?php bloginfo('template_url'); ?>/image/logo.png">
					</div>
					<h1 class="span5 title">
						<?php bloginfo('name'); ?></br>
						<small class="visible-desktop"><?php bloginfo('description'); ?></small>
					</h1>
					<div class="span6 pull-right align-right search-widget">
						<?php get_search_form(true) ?>
						<?php echo $current_user->display_name; ?> 
						<a href="<?php echo wp_logout_url(); ?>" title="Sair do portal"><i class="icon-off icon-white"></i></a>
					</div>
				</div>
			</div>
		</div><!-- /HEADER -->
		<div class="container">
			<ul class="pages">
				<li class="page_item <?php echo is_home() ? "current_page_item" : ""; ?>">
					<a href="<?php echo home_url('/'); ?>"><i class="icon-home icon-white"></i> Home</a>
				</li>
				<?php wp_list_pages('title_li='); ?>
			</ul>
		</div>